import hw2.Calculator;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;


public class CalculatorTest {
        public Calculator calc;

    public CalculatorTest(){
        this.calc = new Calculator();
    }

    @Test
    public void add_addsNumbers_5(){
        Assertions.assertEquals(5, this.calc.add(2, 3));
    }

    @Test
    public void subtract_subtractsNumbers_3(){
        Assertions.assertEquals(3, this.calc.subtract(6, 3));
    }

    @Test
    public void multiply_multipliesNumbers_20(){
        Assertions.assertEquals(20, this.calc.multiply(4, 5));
    }

    @Test
    public void divide_dividesNumbers_8() throws Exception {
        Assertions.assertEquals(8, this.calc.divide(16, 2));
    }

    @Test
    public void divide_ThrowsException_exception(){
        Exception exception = Assertions.assertThrows(Exception.class, () -> this.calc.divide(3, 0));

        String expectedMessage = "Division by zero!";
        String actualMessage = exception.getMessage();

        // ASSERT 2
        Assertions.assertEquals(expectedMessage, actualMessage);
    }
}
